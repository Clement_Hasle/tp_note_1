using System;
using Xunit;
using CompteurLib;

namespace XUnitTestProject1
{
    public class TestCompteur
    {
        [Fact]
        public void TestIncremente()
        {
            CompteurClass.incremente();
            Assert.Equal(1, CompteurClass.getValeur());
            CompteurClass.incremente();
            Assert.Equal(2, CompteurClass.getValeur());
        }
        [Fact]
        public void TestDecremente()
        {
            CompteurClass.decremente();
            Assert.Equal(-1, CompteurClass.getValeur());
            CompteurClass.decremente();
            Assert.Equal(-2, CompteurClass.getValeur());
        }
        [Fact]
        public void TestRaZ()
        {
            CompteurClass.decremente();
            CompteurClass.RaZ();
            Assert.Equal(0, CompteurClass.getValeur());
            CompteurClass.incremente();
            CompteurClass.RaZ();
            Assert.Equal(0, CompteurClass.getValeur());
        }
    }
}
