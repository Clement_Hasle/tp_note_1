﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteurLib
{
    public class CompteurClass
    {
        private static int valeur=0;
        public static void incremente()
        {
            valeur++;
        }
        public static void decremente()
        {
            valeur--;
        }
        public static void RaZ()
        {
            valeur = 0;
        }
        public static int getValeur()
        {
            return valeur;
        }
    }
}
