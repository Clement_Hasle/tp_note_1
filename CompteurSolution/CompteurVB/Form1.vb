﻿Imports CompteurLib
Public Class Form1
    Private Sub btn_decremente_Click(sender As Object, e As EventArgs) Handles btn_decremente.Click
        CompteurLib.CompteurClass.decremente()
        lbl_valeurCompteur.Text = CompteurLib.CompteurClass.getValeur()
    End Sub

    Private Sub btn_incremente_Click(sender As Object, e As EventArgs) Handles btn_incremente.Click
        CompteurLib.CompteurClass.incremente()
        lbl_valeurCompteur.Text = CompteurLib.CompteurClass.getValeur()
    End Sub

    Private Sub btn_RaZ_Click(sender As Object, e As EventArgs) Handles btn_RaZ.Click
        CompteurLib.CompteurClass.RaZ()
        lbl_valeurCompteur.Text = CompteurLib.CompteurClass.getValeur()
    End Sub
End Class
