﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_valeurCompteur = New System.Windows.Forms.Label()
        Me.btn_decremente = New System.Windows.Forms.Button()
        Me.btn_incremente = New System.Windows.Forms.Button()
        Me.btn_RaZ = New System.Windows.Forms.Button()
        Me.tbt_total = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lbl_valeurCompteur
        '
        Me.lbl_valeurCompteur.AutoSize = True
        Me.lbl_valeurCompteur.Location = New System.Drawing.Point(288, 109)
        Me.lbl_valeurCompteur.Name = "lbl_valeurCompteur"
        Me.lbl_valeurCompteur.Size = New System.Drawing.Size(16, 17)
        Me.lbl_valeurCompteur.TabIndex = 0
        Me.lbl_valeurCompteur.Text = "0"
        '
        'btn_decremente
        '
        Me.btn_decremente.Location = New System.Drawing.Point(68, 91)
        Me.btn_decremente.Name = "btn_decremente"
        Me.btn_decremente.Size = New System.Drawing.Size(129, 47)
        Me.btn_decremente.TabIndex = 1
        Me.btn_decremente.Text = "-"
        Me.btn_decremente.UseVisualStyleBackColor = True
        '
        'btn_incremente
        '
        Me.btn_incremente.Location = New System.Drawing.Point(389, 96)
        Me.btn_incremente.Name = "btn_incremente"
        Me.btn_incremente.Size = New System.Drawing.Size(137, 42)
        Me.btn_incremente.TabIndex = 2
        Me.btn_incremente.Text = "+"
        Me.btn_incremente.UseVisualStyleBackColor = True
        '
        'btn_RaZ
        '
        Me.btn_RaZ.Location = New System.Drawing.Point(222, 191)
        Me.btn_RaZ.Name = "btn_RaZ"
        Me.btn_RaZ.Size = New System.Drawing.Size(145, 46)
        Me.btn_RaZ.TabIndex = 3
        Me.btn_RaZ.Text = "RaZ"
        Me.btn_RaZ.UseVisualStyleBackColor = True
        '
        'tbt_total
        '
        Me.tbt_total.AutoSize = True
        Me.tbt_total.Location = New System.Drawing.Point(276, 42)
        Me.tbt_total.Name = "tbt_total"
        Me.tbt_total.Size = New System.Drawing.Size(40, 17)
        Me.tbt_total.TabIndex = 4
        Me.tbt_total.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(591, 303)
        Me.Controls.Add(Me.tbt_total)
        Me.Controls.Add(Me.btn_RaZ)
        Me.Controls.Add(Me.btn_incremente)
        Me.Controls.Add(Me.btn_decremente)
        Me.Controls.Add(Me.lbl_valeurCompteur)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_valeurCompteur As Label
    Friend WithEvents btn_decremente As Button
    Friend WithEvents btn_incremente As Button
    Friend WithEvents btn_RaZ As Button
    Friend WithEvents tbt_total As Label
End Class
